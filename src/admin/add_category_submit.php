<?php
	error_reporting(E_ALL);

    //output buffering
    ob_start();
	//include header
    include '../includes/header.php';

    //check access level
    if(!isset($_SESSION['access_level']) || $_SESSION['access_level'] != 5)
    {
            header("Location: /cvwo-blog/src/index.php");
            exit;
    }
    else
    {
		//check posted input
		if(isset($_SESSION['form_token'], $_POST['form_token'], $_POST['blog_category_name']) && preg_match('/^[a-z][a-z\d_ ,]{2,49}$/i', $_POST['blog_category_name']) !== 0)
		{
			//include db connection
			include '../includes/conn.php';

			if($db) //if successful
			{
				//escape string to prevent injection
				$blog_category_name = mysqli_real_escape_string($link, $_POST['blog_category_name']);

				//sql query
				$sql = "INSERT INTO blog_categories (blog_category_name) VALUES ('{$blog_category_name}')";

				//run the query 
				if(mysqli_query($link, $sql))
				{
					//unset the session token 
					unset($_SESSION['form_token']);
					echo '<h4 class="text-success col-md-8 col-md-offset-2" >Category added.</h4>';
				}
				else
				{
					echo '<h4 class="text-danger col-md-8 col-md-offset-2" >Category not added.</h4>';
				}
			}
			else
			{
				echo '<h4 class="text-danger col-md-8 col-md-offset-2" >Unable to process form.</h4>';
			}
		}
		else
		{
			echo '<h4 class="text-danger col-md-8 col-md-offset-2">Invalid submission.</h4>';
		}
	}
	include '../includes/footer.php';
	//flush output buffer
	ob_end_flush();
?>
