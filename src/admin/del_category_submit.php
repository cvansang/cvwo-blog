<?php
	error_reporting(E_ALL);

	//begin output buffering
	ob_start();

	//include the header 
	include '../includes/header.php';

	//check access level: if not logged in or not admin
	if(!isset($_SESSION['access_level']) || $_SESSION['access_level'] != 5)
	{
		header("Location: /cvwo-blog/src/index.php"); //send user back to index.php
		exit;
	}
	else //if logged in as admin
	{
		//check the form has been posted and the session variable is set 
		if(isset($_SESSION['form_token'], $_POST['blog_category_id']) && is_numeric($_POST['blog_category_id']))
		{

			//include the db connection
			include '../includes/conn.php';

			//if connection is valid
			if($db)
			{
				//escape input to prevent injectino
				$blog_category_id = mysqli_real_escape_string($link, $_POST['blog_category_id']);

				//create the query
				$sql = "DELETE FROM blog_categories 
						WHERE blog_category_id = $blog_category_id";

				//run the query
				if(mysqli_query($link, $sql)) //if succesful
				{
					//unset the session token
					unset($_SESSION['form_token']);

					//retrieve affected row (deleted category)
					//$link was defined in conn.php: link to the database
					//$affected = mysqli_affected_rows($link);

					echo '<h4 class="text-success col-md-8 col-md-offset-2">Category Deleted </h4>';
				}
				else //if not succesful: report error
				{
					echo '<h4 class="text-warning col-md-8 col-md-offset-2">Category not deleted.</h4>';
				}
			}
			else //if can't connect to db
			{
				echo '<h4 class="text-warning col-md-8 col-md-offset-2">Unable to process form.</h4>';
			}
		}
		else //if the submitted form is invalid
		{
			echo '<h4 class="text-warning col-md-8 col-md-offset-2" >Invalid submission.</h4>';
		}
	}
	include '../includes/footer.php';
	ob_end_flush();
?>
