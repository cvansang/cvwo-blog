<!-- Retrieve category list -->
<?php
	//report all error
	error_reporting(E_ALL);

	//begin output buffering
	ob_start();
	//include header
	include '../includes/header.php';

	//check access level: if not logged in or not admin
	if(!isset($_SESSION['access_level']) || $_SESSION['access_level'] != 5)
	{
		header("Location: /cvwo-blog/src/index.php"); //send user back to index.php
		exit;
	}
	else
	{
		//set a token 
		$form_token = uniqid();
		$_SESSION['form_token'] = $form_token;

		//include the database connection
		include '../includes/conn.php';

		//if db connection is valid
		if($db)
		{
			//SELECT all blog categories
			$sql = "SELECT *
				FROM
				blog_categories";
			//make query
			$result = mysqli_query($link, $sql);
			//if the data is not resource
			/*
			if(!is_resource($result))
			{
				echo 'Unable to get category listing';
			}
			else
			{*/
				//create an empty array
				$categories = array();

				//add the rows in $result to the $categories array
				while($row = mysqli_fetch_array($result))
				{
					$categories[$row['blog_category_id']] = $row['blog_category_name'];
				}
			//}
		}
		else //if db connection is invalid
		{
			echo '<h4 class="text-danger col-md-8 col-md-offset-2">Database connection failed.</h4>';
		}
	}
		
?>
<div class="col-md-8 col-md-offset-2">
<div id='del-category'>
<h2 class='text-info'>Delete Category</h2>
<p>
<?php
	if(sizeof($categories) == 0)	//if there is no category
	{
		echo '<h4 class="text-danger">No categories available.</h4>';
	}
	else
	{
		echo '<h4 class="text-success">Select a category name for deletion.</h4>';
	}
?>
</p>
<!-- Create a drop-down menu to select the category to be deleted, then send info to del_category_submit.php-->
<form action="del_category_submit.php" method="post">
<select multiple name="blog_category_id" class="form-control">
<?php
	foreach($categories as $id=>$cat)	//for each member of the $categories array
	{
		echo "<option value=\"$id\">$cat</option>\n";
	}
?>
</select>
<input type="submit" value="Submit" onclick="return confirm('Delete selected category?')"/>
</form>
</div>
</div>
<?php 
	include '../includes/footer.php'; 
	ob_end_flush();
?>
