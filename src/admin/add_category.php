<?php

	//begin output buffering
	ob_start();
	//include header
	include '../includes/header.php';

	//check access level: if not logged in or not admin
	if(!isset($_SESSION['access_level']) || $_SESSION['access_level'] != 5)
	{
		//echo $_SESSION['access_level'];
		header("Location: /cvwo-blog/src/index.php"); //send user back to index.php
		exit;
	}
	else
	{
		//echo 'at else';
		//set a token 
		$form_token = uniqid();
		$_SESSION['form_token'] = $form_token;
	}
?>
<div class="col-md-8 col-md-offset-2">
<div id='add-category'>
<h2 class='text-info'>Add Category</h2>

<!-- Simple HTML form -->
<form action="add_category_submit.php" method="post">
<input type="hidden" name="form_token" value="<?php echo $form_token; ?>" />
<div class="form-group">
	<input type="text" class="form-control input-lg" name="blog_category_name" placeholder = "Input here"/>
</div>
<input type="submit" value="Add Category" class="btn btn-default"/>
</form>
</div>
</div>
<?php
	include '../includes/footer.php';
	ob_end_flush();
?>
