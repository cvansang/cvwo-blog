<?php
	//include header and connection
	include '../includes/header.php';
	include '../includes/conn.php';

	if($db) //if connection is successful 
	{
		//cid == content id
		if (isset($_GET['cid']))
		{
			$blog_content_id = $_GET['cid'];

			$sql = "SELECT
			blog_content_headline,
			blog_content_text,
			DATE_FORMAT(blog_content_date, '%b %d %Y') AS blog_content_date,
			blog_category_name,
			blog_user_name
			FROM
			blog_content
			JOIN
			blog_users
			USING(blog_user_id)
			JOIN
			blog_categories
			USING(blog_category_id)
			WHERE blog_content_id = '{$blog_content_id}'";

			$result = mysqli_query($link, $sql);
			//create an array to store the entry
			$blog_array = array();

			if(mysqli_num_rows($result) != 0) //if the exists some blog
			{
				//copy blogs onto array
				while($row = mysqli_fetch_array($result, MYSQL_ASSOC))
				{
					$blog_array[] = $row;
				}
			}
		}
		else 
		{
			echo '<h3 class="text-danger">Invalid Entry ID.</h3>';
		}
	}
?>
<style>
	#disqus_thread {
color: #ffffff;
}
</style>
<div class="col-md-8 col-md-offset-2">
<div class='main-entry-block'>
<?php
	if(sizeof($blog_array) > 0) //if there are some entries
	{
		//display blog entries
		foreach($blog_array as $blog)
		{
			echo "<div class='entry'>";
			echo '<div style="text-align:left"><h2><font color= #700000>'.$blog['blog_content_headline'].'</h2></font></div>';
			echo '<h6><p><span class="text-muted"><font color= #E0E0E0>Added by '.$blog['blog_user_name'].' on '.$blog['blog_content_date'].'</font></p></h6>';
			echo '<h6><span class="text-muted"><font color= #E0E0E0>'.'Category: '.$blog['blog_category_name'].'</font></span></h6>';
			echo '<font color=FFFFFF><p>'.$blog['blog_content_text'].'</font></p>';
			echo "</div>";
		}
	}
	else
	{
		echo '<h3 class="text-danger">No Entry Found</h3>';
	}
	echo '<h4 class="text-muted"><font color= #E0E0E0> Feel free to comment below. </font></h4>';
?>
			<div id="disqus_thread"></div>
			<script type="text/javascript">
				/* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
				var disqus_shortname = 'cvwoblog'; // required: replace example with your forum shortname

				/* * * DON'T EDIT BELOW THIS LINE * * */
				(function() {
				var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
				dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
				(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
				})();
			</script>
			<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
</div>
</div>
</div>
<?php
	//include the footer file
	include '../includes/footer.php';

?>