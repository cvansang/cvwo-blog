<?php
	//output buffering 
	ob_start();

	//include header
	include '../includes/header.php';

	//check access level
	if(!isset($_SESSION['access_level']))
	{
		header("Location: ../users/login.php");
		exit;
	}
	else
	{
		//include db connection 
		include '../includes/conn.php';
		if (!isset($_GET['sort_by'])) 
		{
			$sort_by = 'blog_content.blog_content_id';
		} 
		else 
		{
			$sort_by = $_GET['sort_by'];
		}

		if($db) //successful
		{
			//select all blog entries
			$sql = "SELECT
				blog_content.blog_content_id,
				blog_content.blog_content_headline,
				blog_content.blog_content_date,
				blog_users.blog_user_name,
				blog_categories.blog_category_name
				FROM
				blog_content
				INNER JOIN blog_users ON blog_content.blog_user_id = blog_users.blog_user_id
				INNER JOIN blog_categories ON blog_content.blog_category_id = blog_categories.blog_category_id
				";
			//if user is not an admin
			if($_SESSION['access_level'] == 1)
			{
				$blog_user_id = mysqli_real_escape_string($link, $_SESSION['blog_user_id']);
				$sql .= " WHERE blog_content.blog_user_id = $blog_user_id";
			}
			$sql .= " ORDER BY ".$sort_by." ASC";
			//run the query
			$result = mysqli_query($link, $sql) or die(mysqli_error($link));

			//if there are entries, put them into an array
			if(mysqli_num_rows($result) != 0)
			{
				$cont = true;
				$blog_array = array();
				while($row = mysqli_fetch_array($result, MYSQL_ASSOC))
				{
					$blog_array[] = $row;
				}
			}
			else
			{
				$cont = false;
				$message = '<h4 class = "text-danger col-md-8 col-md-offset-2"><font color=#E0E0E0>No Blog Entries Found</font></h4>';
			}
		}
		else
		{
			$cont = false;
			$message ='<h4 class = "text-danger col-md-8 col-md-offset-2"><font color=#E0E0E0>No Connection Available</font></h4>';
		}
	}
?>
<style>
	.form-control {
		width: 100%;
	
	}
	th {
	 	color: #CC0000;
	}
</style>
<div class="col-md-8 col-md-offset-2">
<div id='list-blog'>
<h2 class="text-primary">CVWO Blog</h2>

<h4 class="text-primary"> Sort by: </h4> 
<!-- List out options, display the lastest chosen option -->
<div class="row">
<div class="col-md-2">
<form action="list_blogs.php" method="get">
	<select name='sort_by' class="form-control input-sm">
		<option value="blog_content.blog_content_id" 
			<?php if($sort_by=='blog_content.blog_content_id') {echo " selected";}?> > Default </option>
		<option value="blog_users.blog_user_name"
			<?php if($sort_by=='blog_users.blog_user_name') {echo " selected";}?> > Author </option>
		<option value="blog_content.blog_content_headline"
			<?php if($sort_by=='blog_users.blog_content_headline') {echo " selected";}?> > Title </option>
		<option value="blog_content.blog_content_date"
			<?php if($sort_by=='blog_content.blog_content_date') {echo " selected";}?> > Date Created </option>
		<option value="blog_categories.blog_category_name"
			<?php if($sort_by=='blog_categories.blog_category_name') {echo " selected";}?>>  Category </option>
	</select>
	<input type="submit" class="btn btn-primary btn-sm" value="Submit"/>
</form>
</div>
</div>
<br>
<!-- Create a table listing every entries -->

<table class="table table-hover">
<thead><tr>
		<th>Title</th>
		<th>Category</th>
		<th>Author</th>
		<th>Date Created</th>
		<th colspan="2">Action</th>
</tr></thead>
<tfoot></tfoot>
<tbody>
<?php
	if($cont) 
	{
		//display entries info on the table
		foreach($blog_array as $blog)
		{
			echo '<tr>
			<td><a href="viewpost.php?cid='.$blog['blog_content_id'].'">'.$blog['blog_content_headline'].'</a></td>
			<td>'.$blog['blog_category_name'].'</td>
			<td>'.$blog['blog_user_name'].'</td>
			<td>'.$blog['blog_content_date'].'</td>
			<td><a href="../users/edit_blog.php?bid='.$blog['blog_content_id'].'">Edit</a></td>
			<td><a href="../users/delete_blog.php?bid='.$blog['blog_content_id'].'" onclick="return confirm(\'Are you sure?\')">Delete</a></td>
			</tr>';
		}
	}
	else {
		echo $message;
	}
?>
</tbody>
</table>
</div>
</div>
<?php
	include '../includes/footer.php';
?>
