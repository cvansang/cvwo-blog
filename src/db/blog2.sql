-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 05, 2015 at 11:04 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `blog2`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog_categories`
--

CREATE TABLE IF NOT EXISTS `blog_categories` (
`blog_category_id` int(11) NOT NULL,
  `blog_category_name` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog_categories`
--

INSERT INTO `blog_categories` (`blog_category_id`, `blog_category_name`) VALUES
(6, 'Testing'),
(7, 'Development'),
(8, 'Random'),
(9, 'Dota 2'),
(10, 'Technology'),
(11, 'Music');

-- --------------------------------------------------------

--
-- Table structure for table `blog_content`
--

CREATE TABLE IF NOT EXISTS `blog_content` (
`blog_content_id` int(11) NOT NULL,
  `blog_category_id` int(11) NOT NULL,
  `blog_user_id` int(11) NOT NULL,
  `blog_content_headline` varchar(255) NOT NULL,
  `blog_content_text` text NOT NULL,
  `blog_content_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `blog_publish` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog_content`
--

INSERT INTO `blog_content` (`blog_content_id`, `blog_category_id`, `blog_user_id`, `blog_content_headline`, `blog_content_text`, `blog_content_date`, `blog_publish`) VALUES
(3, 10, 11, 'Come for lulz, stay for hacktivism: a new book on Anonymous, reviewed', '<p>Circa 2010 and 2011, a year or so before I joined the staff of Ars Technica, I had followed the online antics of Anonymous from a distance. I knew the rough outline of Anonymous, its initial motives (&ldquo;for the lulz&rdquo;) and its consequences, such as the legendary (and hilarious) <a href="http://arstechnica.com/tech-policy/2011/02/anonymous-speaks-the-inside-story-of-the-hbgary-hack/">hack of security firm HBGary Federal</a>, as reported in these hallowed pages.</p><br />\r\n<p>But what I didn&rsquo;t fully grasp until now was the full, complex and rich play-by-play story provided by somebody who knows the group as well as any bona fide Anon: <a href="http://gabriellacoleman.org/">Biella Coleman</a>, an anthropology professor at McGill University. Her new book, <a href="http://www.amazon.com/Hacker-Hoaxer-Whistleblower-Spy-Anonymous/dp/1781685835">Hacker, Hoaxer, Whistleblower, Spy</a> deftly chronicles the rise of Anonymous, and the fall of many of its most prominent members.</p><br />\r\n<p>The tome details her time embedding with Anonymous in its IRC lairs, and even meets a few of them in person, including the recently released government informant Hector Xavier Monsegur, better known by his online handle, Sabu. (Who knew he was gluten-free?)</p><br />\r\n<p>Coleman&rsquo;s reporting is comprehensive&mdash;she quotes at length from IRC transcripts, and brings in other academic and anthropological sources not commonly found in other tech books. Her detailed account is certainly compelling, particularly for someone like me who only had a passing knowledge of Anonymous, but by the end of the book, I was left wondering: what now?</p><br />\r\n<p>Following the arrests of a notable handful of the most public leaders of the group, it seems that the group and its followers have largely gotten the wind knocked out of its sails. Yes, there have been small-scale &ldquo;ops&rdquo; over the last two years, but 2014&rsquo;s &ldquo;Operation Ferguson&rdquo; resulted in the publication of a police officer who had <a href="http://www.washingtonpost.com/blogs/the-switch/wp/2014/08/14/how-anonymous-got-it-right-and-wrong-in-ferguson/">nothing do with the killing of Michael Brown</a>.</p><br />\r\n<p>Like Adrian Chen&rsquo;s <a href="http://www.thenation.com/article/190369/truth-about-anonymouss-activism">December 2014 review</a> of Hacker, Hoaxer, I too am not convinced of the staying power of Anonymous&rsquo; hacktivism. While I&rsquo;m sure that Anonymous, in some form or another is still kicking around in the bowels of IRC, but I&rsquo;m not certain that its physical protests, online ops, DDOS attacks, have had many (if any) lasting effects. After all, despite its famously decentralized nature, a number of its prominent members have been arrested. One former member, Barrett Brown, is due to be <a href="http://arstechnica.com/tech-policy/2014/04/former-anon-spokesman-barrett-brown-copping-a-federal-guilty-plea/">sentenced</a> in federal court later this month after taking a plea deal nine months ago.</p><br />\r\n<p>Coleman compiles a laundry list of ops, and boldly concludes early on that Anonymous &ldquo;could be found at the heart of hundreds of political &lsquo;ops&rsquo;&mdash;becoming integral, even, to some of the most compelling political struggles of our age.&rdquo; To wit, in 2014 and now 2015, Anonymous has had zero influence on the Syrian civil war, the conflict in Ukraine, or the fight against Islamic State, to name a few.</p><br />\r\n<h2>Moralfags v. lulz-seekers</h2><br />\r\n<p>The second chapter of Hacker, Hoaxer details 2008&rsquo;s Project Chanology, the first real Anonymous op. Like many people, I hadn&rsquo;t heard of Anonymous until that now-famous creepy video, &ldquo;<a href="https://www.youtube.com/watch?v=JCbKv9yiLiQ">Message to Scientology</a>,&rdquo; hit YouTube. April 2008 marked the apotheosis of protests against the Church of Scientology, with protests in many cities worldwide.</p><br />\r\n<p>Among other things, Coleman uses the Project Chanology episode as an occasion to explore the &ldquo;binary between moralfags and &lsquo;hardened&rsquo; lulz-seekers&rdquo; in Anonymous&rsquo; core roots. While many online activists first found Anonymous as part of its anti-Scientology flare-up, the book also details how easily lulz can go dark.</p><br />\r\n<p>She quotes an Anon whom she gives the IRC handle &ldquo;CPU&rdquo; as saying on March 16, 2008:</p><br />\r\n<blockquote><br />\r\n<p>We should just hit a random forum for the lulz. Anyone remember the emetophobia raids?</p><br />\r\n<p>I&rsquo;m searching for a forum lol.</p><br />\r\n<p>oh lol <a href="../../../www.suicideforum.com/">http:///www.suicideforum.com/</a></p><br />\r\n<p>First person to push someone to the edge wins?</p><br />\r\n<p>&hellip;</p><br />\r\n<p>Or we could find an epilepsy forum and spam it with flashing gifs or something?</p><br />\r\n<p><a href="http://www.epilepsyforum.org.uk">http://www.epilepsyforum.org.uk</a></p><br />\r\n<p>gogogogogo</p><br />\r\n</blockquote><br />\r\n<p>In 2012, someone made good on this suggestion (Coleman dubs it &ldquo;morally reprehensible&rdquo;), but she uses the dichotomy between the Anonymous&rsquo; fun-seeking roots and its morphing into a politically motivated group that can be riled up once it finds the right target.</p><br />\r\n<blockquote><br />\r\n<p>Anonymous is not a united front, but a hydra&mdash;comprising numerous different networks. Even within a single project there are working groups that are often at odds with one another&mdash;not to mention the civil wars between different nodes of Anonymous more generally. But even if Anons don&rsquo;t always agree about what is being done under the auspices of Anonymous, they tend to respect the fact that anyone can assume the moniker. The mask, which has becomes its signature icon, functions as an eternal beacon, broadcasting the symbolic value of equality, even in the face of bitter divisions and inequalities.</p><br />\r\n</blockquote><br />\r\n<p>While obviously the Church of Scientology still persists, Coleman concludes that Anonymous &ldquo;altered the game so fundamentally that critics could now stand confidently under the sun without fear of reprisal.&rdquo; While I&rsquo;m hardly an expert on Scientology, there have been numerous protests, articles, and books written on the subject well before and well after Anonymous came along.</p><br />\r\n<h2>Arrests galore</h2><br />\r\n<p>Similarly, over four years ago, the Tunisian Revolution began&mdash;sparked by a produce vendor in a small town who set himself on fire. A handful of online Tunisians &ldquo;spammed the shit out of the link to the channel #optunisia everywhere&rdquo; as a way to get others interested in the brewing protests in their home country.</p><br />\r\n<p>In the end, Anons lead the charge in DDOSing Tunisian government websites, and providing some (admittedly useful) digital care packages, including Greasemonkey scripts and copies of Tor. While the group did wage its relatively tiny battles online, absent from the story is any mention of large-scale professional strikes by Tunisia''s professional class of lawyers and teachers, culminating in the fleeing of President Ben Ali on January 14, 2011. Coleman nevertheless trumpets that Anonymous "achieved so much" in the North African state. (Just last month, <a href="http://www.economist.com/news/leaders/21636748-has-been-bad-year-nation-states-someand-one-particulardeserve">The Economist</a> named Tunisia as its country of the year.)</p><br />\r\n<p>But the core of the book is the telling of Anonymous&rsquo; most epic win: the hacking of HBGary Federal, in February 2011.</p><br />\r\n<aside><br />\r\n<h3>Further Reading</h3><br />\r\n<div><a href="http://arstechnica.com/tech-policy/2011/02/how-one-security-firm-tracked-anonymousand-paid-a-heavy-price/"><img src="http://cdn.arstechnica.net/wp-content/uploads/2011/02/anonymous-egg-list.jpg" alt="" /></a><br />\r\n<h2><a href="http://arstechnica.com/tech-policy/2011/02/how-one-security-firm-tracked-anonymousand-paid-a-heavy-price/">How one man tracked down Anonymous&mdash;and paid a heavy price</a></h2><br />\r\n<p>Aaron Barr, CEO of security firm HBGary Federal, spent a month tracking down &hellip;</p><br />\r\n</div><br />\r\n</aside><br />\r\n<p>As <a href="http://arstechnica.com/tech-policy/2011/02/how-one-security-firm-tracked-anonymousand-paid-a-heavy-price/">Ars Technica</a> reported at the time (and in greater detail in the e-book <a href="http://www.amazon.com/Unmasked-Peter-Bright-ebook/dp/B004TCLKK4">Unmasked</a>), Aaron Barr, the CEO of HBGary Federal, a semi-obscure security company, announced that he had linked the online handles of Anons to their real identities and was preparing to share that information with federal authorities. Instead, Barr and HBGary Federal got hacked themselves, with Anonymous taking over 70,000 company e-mails and other documents.</p><br />\r\n<p>For someone like me who had only a passing familiarity with the story as a whole, it was interesting to see and read first-hand the now-famous transcripts that include the line: &ldquo;keep in mind we have all your e-mails.&rdquo;</p><br />\r\n<p>So what happened to HBGary Federal (and its corporate parent, HBGary) after its epic fail? By February 2012, the company was <a href="http://investor.mantech.com/releasedetail.cfm?ReleaseID=652099">sold to ManTech</a>, another digital security firm. Presumably its digital defenses have been noticeably hardened&mdash;but nevertheless, the underlying corporate goal remains the same as it ever was.</p><br />\r\n<p>By the end of the book (and many ops later) Coleman implies that Anonymous belongs in the pantheon of Internet heros&mdash;like Julian Assange, the Electronic Frontier Foundation and Edward Snowden.</p><br />\r\n<p>While the story of Anonymous is fueled by lulz, it seems that the group has largely just fizzled out.</p><br />\r\n<p>As Ars'' own Peter Bright wrote in March 2012:</p><br />\r\n<blockquote><br />\r\n<p>28-year-old Hector Xavier "Sabu" Monsegur was arrested by federal agents in June last year, and has since been <a href="http://arstechnica.com/tech-policy/news/2012/03/report-lulzsec-leader-sabu-worked-with-fbi-since-last-summer.ars">co-operating with the FBI</a>. That co-operation led to the capture of <a href="http://arstechnica.com/tech-policy/news/2011/09/kayla-taken-down-in-latest-lulzsec-arrests.ars">Ryan "Kayla" Ackroyd</a>, 23, <a href="http://arstechnica.com/security/news/2011/07/key-lulzsec-figure-nabbed-as-legal-attack-on-paypal-launched.ars">Jake "Topiary" Davis</a>, 19, and <a href="http://arstechnica.com/security/news/2011/07/fbi-arrests-16-anons-across-us-uk-police-pick-up-lulzsec-member.ars">unnamed teenager "tflow"</a>, 16, in the UK for, among other crimes, their participation in the HBGary hack. Darren "pwnsauce" Martyn, 19, in Ireland, has been named and indicted, but <a href="http://www.irishexaminer.com/world/kfidgbcwidau/">not yet arrested</a>.</p><br />\r\n<p>The HBGary hackers collectively called themselves Internet Feds. They then started working under the name LulzSec, rapidly achieving infamy for a series of high-profile break-ins (victims including <a href="http://arstechnica.com/tech-policy/news/2011/05/hacktivists-scorch-pbs-in-retaliation-for-wikileaks-documentary.ars">PBS</a>, <a href="http://arstechnica.com/tech-policy/news/2011/06/sony-hacked-yet-again-plaintext-passwords-posted.ars">Sony</a>, and <a href="http://arstechnica.com/security/news/2011/06/lulz-security-takes-on-nintendo-fbi-sony-fbi-fights-back.ars">Nintendo</a>) and <a href="http://arstechnica.com/tech-policy/news/2011/06/titanic-takeover-tuesday-lulzsecs-busy-day-of-hacking-escapades.ars">denial-of-service attacks</a>. But by late September 2011, everyone in LulzSec except one member, <a href="http://arstechnica.com/tech-policy/news/2012/03/everything-incriminating-has-been-burned-anons-fight-panic-after-sabu-betrayal.ars">avunit</a>, had been identified, and every identified member except pwnsauce had been arrested.</p><br />\r\n</blockquote>', '2015-01-04 13:23:24', 0),
(4, 11, 11, 'Apocalyptic Love', '<p><img style="display: block; margin-left: auto; margin-right: auto;" src="http://i.ytimg.com/vi/bA7LW6g2ayQ/maxresdefault.jpg" alt="Hi" width="538" height="302" /></p><br />\r\n<p><font class="s">Released:</font> May 22, 2012 <font class="s">Genre:</font> Hard Rock <font class="s">Label:</font> Dik Hayd International <font class="s">Number Of Tracks:</font> 13 This is Slash''s second solo record, and this time Myles Kennedy is handling 100% of the vocals. As a matter of a fact, Slash has said that this album is more of a collaborative effort with Miles Kennedy than a solo album.</p><br />\r\n<p>&nbsp;</p><br />\r\n<p style="white-space: pre-line;"><span class="s">Sound: </span>We all know and love <strong>Slash</strong>. This is the guy behind the riffs from "<strong>Paradise City</strong>" and "<strong>Welcome To The Jungle</strong>". This is the guy from the "<strong>November Rain</strong>" video soloing in the desert. He''s become almost a trademark in and of himself with his long hair obscuring his face under his top hat. There are very few guitarists who can say they haven''t played a few riffs, solos or licks made famous by <strong>Slash</strong>. From the streets of Los Angeles, <strong>Slash</strong> and his bandmates in <strong>Guns N Roses</strong> became celebrity bad boys and arena rock superstars basically overnight. At the time, they were just what rock music needed a solid kick in the butt. The music scene was becoming more and more polished with the artists at the time wearing makeup and having elaborate hairdos. <strong>Guns N Roses</strong> looked rough, and they were passionate and their songs rocked. From the very beginning <strong>Axl</strong> and <strong>Slash</strong> stood out as the dominant personalities in the band and it was only a matter of time till the band blew up, but while it lasted it was intense and vital. Of course, <strong>Guns N Roses</strong> did implode with all kinds of inner strife, drug binges, rehab, and out of control egos. Fast forward to the present and while <strong>Axl</strong> is continuing on with new members as <strong>Guns N Roses</strong>, we have <strong>Slash</strong> creating some pretty solid music which is pretty remarkable when you think that this guy''s career should have peaked like 20 years ago. After the "demise" of <strong>Guns N Roses</strong>, <strong>Slash</strong> attempted a band called <strong>Slash''s Snakepit</strong> which was met with mostly a negative reaction from fans and critics. After this, <strong>Slash</strong> regrouped and formed <strong>Velvet Revolver</strong>, which released its first album, "<strong>Contraband</strong>", in 2004. "<strong>Libertad</strong>" followed in 2007, but then there were issues again with an egoistical vocalist namely <strong>Scott Weiland</strong>. After this <strong>Velvet Revolver</strong> was kind of put on hold as they attempt to find a new vocalist. In the meantime, <strong>Slash</strong> began his solo career and released his first album, which was self-titled, in 2010. He had numerous guest vocalists everyone from <strong>Lemmy</strong>, <strong>Ozzy</strong>, <strong>Fergie</strong> and <strong>Myles Kennedy</strong> (among others). The album was a success and received generally favorable reviews. The first single from the album, "<strong>By The Sword</strong>", stomped ass with a steam punk meets Mad Max music video. Since then, <strong>Slash</strong> has been busy touring, writing and recording and his second release, "<strong>Apocalyptic Love</strong>", is finally releasing. This time around <strong>Slash</strong> is sticking to a single vocalist <strong>Myles Kennedy</strong> which gives "<strong>Apocalyptic Love</strong>" a much more cohesive feel than his previous release. <strong>Slash</strong> has said that "<strong>Apocalyptic Love</strong>" is much more of a collaboration with <strong>Myles Kennedy</strong> than another solo album. As to the sound of the album, what can I say? This is heavy blues based rock, which is what we''ve come to expect from <strong>Slash</strong>, with a few minor detours. The solos are awesome; his guitar tone is the epic Marshall crunch tone that no one can seem to reproduce. Most of the songs are very riff-oriented and you will find yourself tapping your foot or nodding your head to the music. While I don''t think any of the songs are as memorable as the songs off of "<strong>Appetite For Destruction</strong>", I think a large part of this is just that the music climate/industry is so different now and the timing of the release of "<strong>Appetite For Destruction</strong>" was like the perfect storm. Don''t take any statement I''ve made as detraction from "<strong>Apocalyptic Love</strong>", because this album is absolutely solid. <span class="s"> //&nbsp;9</span></p><br />\r\n<p>&nbsp;</p><br />\r\n<p style="white-space: pre-line;"><span class="s">Lyrics: </span><strong>Myles Kennedy</strong> initially worked with <strong>Slash</strong> on his self-titled album and then spent a lot of time on tour with <strong>Slash</strong> for that album, and when <strong>Slash</strong> sat down to record his second album he had worked up a good working relationship with <strong>Myles</strong> and so he provided all the vocals on this album, as well as playing rhythm guitar. <strong>Myles</strong> isn''t your average rock vocalist as in his voice is unique and is recognizable. I mean, this is the guy who almost replaced <strong>Robert Plant</strong> in <strong>Led Zeppelin</strong> he can sing. <strong>Myles</strong> was in a few small bands but eventually was lead guitarist and vocalist for a band he formed with friends called <strong>The Mayfield Four</strong>, which released two albums. After that he sent in an audition tape for (or was asked to audition, accounts vary) <strong>Velvet Revolver</strong> but never showed up for the physical audition. After this he joined <strong>Alter Bridge</strong> where he really made his name. After almost becoming the new vocalist for <strong>Led Zeppelin</strong> and recording with <strong>Slash</strong>, that brings us up to present. His vocal delivery is passionate and precise he really is just an awesome blues rock vocalist and maybe the best to come out since the 70''s. They lyrics on the album fit the music and genre, and I believe they were mostly if not completely written by <strong>Myles Kennedy</strong>. As a sample of the lyrics, here are some from the title track, "<strong>Apocalyptic Love</strong>": "<em>I got a real bad feeling/ They''ll be nothing to say/ Whole world''s going crazy/ know the end is on the way/ but we''ve got one hot minute/ to do anything you like/ to do anything you like/ This is the last time oh oh/ don''t care how we do it/ just as long as we can love under the same sky/ all we got is tonight/ I wanna feel you fallin'' all up my thigh/ Apocalyptic love/ I wanna ride until we die/ until the last night''s faded/ I just wanna see you smile/ I just wanna see you smile/ as I go downtown</em>". Nothing more blues rock than that. Really, listening to the album and the previous lyrics being a random sample, I would say the lyrics from "<strong>Apocalyptic Love</strong>" are some of the most mediocre lyrics from the album. There are some really inspired lyrics in places and the vocal delivery is amazing throughout. <span class="s"> //&nbsp;9</span></p><br />\r\n<p>&nbsp;</p><br />\r\n<p style="white-space: pre-line;"><span class="s">Overall Impression: </span>When the song "<strong>Anastasia</strong>" started for the first time I swear my ears perked up. When the acoustic intro was over and the little classical shred passage on electric guitar started I think my mouth was watering. Somehow I don''t think I''ve heard neo-classical mixed with blues based rock since that <strong>Ralph Macchio</strong> movie "<strong>Crossroads</strong>" back in the day. Right now, my absolute favorite track on the album is "<strong>Anastasia</strong>". I''m also really digging "<strong>Halo</strong>" and "<strong>Not For Me</strong>". Of course, the single "<strong>You''re A Lie</strong>" isn''t bad either, but it isn''t my favorite. I think with <strong>Slash</strong>''s previous self-titled album he was maybe cleaning out some old ideas (which were still awesome) but with "<strong>Apocalyptic Love</strong>" we''re getting much more current ideas from him. "<strong>Bad Rain</strong>" is also a solid track, and has possibly the most memorable solo on the album. There are 13 tracks on the album with a total run time of just over 54 minutes. There is a deluxe edition with two more tracks that bumps the album to just over an hour. I''m several listens in to the album at this point and the songs are only growing on me even more as I go. <strong>Slash</strong> is one of those musicians who don''t ever seem to get the attention they deserve, regardless of the fact that his image is universally recognized. I''m really glad we got this second solo release from <strong>Slash</strong> and hope to see a third. They just seem to get better with each release so far. I grew up with <strong>Guns N Roses</strong> and it profoundly affected me when they broke up. Since then I''ve followed <strong>Slash</strong> pretty closely and in the past years have begun to become excited with what he is doing once again and I hope he keeps it up. <span class="s"> //&nbsp;9</span></p><br />\r\n<p>&nbsp;</p><br />\r\n<p><em>- Brandon East (c) 2012</em></p><br />\r\n<p>&nbsp;</p>', '2015-01-05 09:19:30', 0);

-- --------------------------------------------------------

--
-- Table structure for table `blog_users`
--

CREATE TABLE IF NOT EXISTS `blog_users` (
`blog_user_id` int(11) NOT NULL,
  `blog_user_name` varchar(20) NOT NULL,
  `blog_user_password` char(40) NOT NULL,
  `blog_user_email` varchar(254) NOT NULL,
  `blog_user_access_level` int(1) NOT NULL DEFAULT '0',
  `blog_user_status` varchar(13) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog_users`
--

INSERT INTO `blog_users` (`blog_user_id`, `blog_user_name`, `blog_user_password`, `blog_user_email`, `blog_user_access_level`, `blog_user_status`) VALUES
(10, 'shinsing', '9ff3e04a14876beec13f6b49aecad0bc2505f4e5', 'sangvanchau@gmail.com', 1, '54a8bc978a086'),
(11, 'master', '4576bc95e549efb258913799daf27781480b61e8', 'cvansang94@gmail.com', 5, '54a8bdd78c680'),
(12, 'vansang', '9ff3e04a14876beec13f6b49aecad0bc2505f4e5', 'bar@foo.com', 1, ''),
(13, 'cvansang94', '5da54a5c3291f4bc09d547b3fc56b3b492a545ad', 'bar@bar.com', 1, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog_categories`
--
ALTER TABLE `blog_categories`
 ADD PRIMARY KEY (`blog_category_id`);

--
-- Indexes for table `blog_content`
--
ALTER TABLE `blog_content`
 ADD PRIMARY KEY (`blog_content_id`), ADD KEY `blog_user_id` (`blog_user_id`), ADD KEY `blog_category_id` (`blog_category_id`);

--
-- Indexes for table `blog_users`
--
ALTER TABLE `blog_users`
 ADD PRIMARY KEY (`blog_user_id`), ADD UNIQUE KEY `blog_username` (`blog_user_name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog_categories`
--
ALTER TABLE `blog_categories`
MODIFY `blog_category_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `blog_content`
--
ALTER TABLE `blog_content`
MODIFY `blog_content_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `blog_users`
--
ALTER TABLE `blog_users`
MODIFY `blog_user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `blog_content`
--
ALTER TABLE `blog_content`
ADD CONSTRAINT `blog_content_ibfk_1` FOREIGN KEY (`blog_user_id`) REFERENCES `blog_users` (`blog_user_id`) ON DELETE CASCADE,
ADD CONSTRAINT `blog_content_ibfk_2` FOREIGN KEY (`blog_category_id`) REFERENCES `blog_categories` (`blog_category_id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
