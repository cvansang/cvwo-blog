<?php

	//output buffering
	ob_start();
	//begin session
	session_start();
	global $base_dir;
	//if the user logged in: unset access_level
	if(isset($_SESSION['access_level']))
	{
		unset($_SESSION['access_level']);
	}

	//redirect to index.php
	header("Location: ../index.php");
	//since access_level is unset, user can sign in again
	
	//flush the buffer
	ob_end_flush();

?>
