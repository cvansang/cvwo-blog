<?php
	//output buffering 
	ob_start();
	//include header
	include '../includes/header.php';

	//if not logged in 
	if(!isset($_SESSION['access_level']) || $_SESSION['access_level'] < 1)
	{
		//redirect to login page
		header("Location: login.php");
		exit;
	}
	else //if logged in
	{
		//create form token to prevent multi-post
		$form_token = uniqid();
		$_SESSION['form_token'] = $form_token;

		//include connection to db
		include '../includes/conn.php';

		//check db connection
		if($db) //success
		{
			//retrieve categories list
			$sql = "SELECT
				blog_category_id,
				blog_category_name
				FROM
				blog_categories";
			$result = mysqli_query($link, $sql);
			
			//if there's some categories
			if(mysqli_num_rows($result) != 0)
			{
				//arrange categories in an array
				$categories = array();
				while($row = mysqli_fetch_array($result))
				{	
					$categories[$row['blog_category_id']] = $row['blog_category_name'];
				}

				//set form values to specify action
				$blog_form_action = 'add_blog_submit.php';
				$blog_heading = "Add A Blog Entry";
				$blog_content_headline = '';
				$blog_content_text = '';
				$blog_form_submit_value = 'Confirm'; //differentiate from editting

				//include the blog form
				include '../includes/blog_form.php';
			}
			else //if there's no categories
			{
				echo '<h4 class="text-danger">No categories found</h4>';
			}
		}
		else //if can't connect to db
		{
			echo '<h4 class="text-danger">Unable to complete request</h4>';
		}
		include '../includes/footer.php';
	}
?>
