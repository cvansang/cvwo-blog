<?php
	//include header
	include '../includes/header.php'; 

	//set a form token
	$_SESSION['form_token'] = md5(rand(time(), true));
?>
<style>
	dt {
		color:#E0E0E0;
	}
</style>
<div class="cold-md-8 col-md-offset-4">
<div id='login-component'>
<span class ="text-info"><h1>Blog Login</h1></span>
<p><font color=#E0E0E0>
Please enter your username and password.
</p></font>
<div class='center-block'>
<form action= "login_submit.php" method="post">	
	<div class = 'form-group'>
		<input type="hidden" name="form_token" value="<?php echo $_SESSION['form_token']; ?>" />
		<dl>
	</div>
	<div class = 'form-group'>
		<dt>Username</dt>
		<dd><input type="text" name="blog_user_name" placeholder="Enter Username" class="form-control"/></dd>
	</div>
	<div class = 'form-group'>
		<dt>Password</dt>
		<dd><input type="password" name="blog_user_password" placeholder="Enter Password" class="form-control"/></dd>
		<br>
		<dd><input type="submit" value="Login" class="btn btn-primary"/></dd>
		</dl>
	</div>
	<!--info will be sent to login_submit.php-->
</form>
</div>
</div>
</div>
</div>
<?php include '../includes/footer.php'; ?>
<!-- include footer -->