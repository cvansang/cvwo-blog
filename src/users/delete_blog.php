<?php
	//begin output buffering
	ob_start();
	include '../includes/header.php';
	//check if the user is logged in
	if(!isset($_SESSION['access_level'], $_SESSION['blog_user_id']))
	{
		header("Location: login.php");
		exit;
	}
	else
	{
		//bid is supplied from list_blog
		if(isset($_GET['bid']) && is_numeric($_GET['bid']))
		{
			//include db connection
			include '../includes/conn.php';

			if($db)
			{
				$blog_content_id = mysqli_real_escape_string($link, $_GET['bid']);

				//create query to delete the row with a certain id
				$sql = "DELETE
					FROM
					blog_content
					WHERE
					blog_content_id = $blog_content_id";

				//check if user is registered
				if($_SESSION['access_level'] == 1)
				{
					$blog_user_id = mysqli_real_escape_string($link, $_SESSION['blog_user_id']);
					$sql .= " AND blog_user_id = $blog_user_id";
				}

				//run the query
				if(mysqli_query($link, $sql))
				{
					unset($_SESSION['form_token']);
					//$affected = mysql_affected_rows($link);
					//redirect user back to list_blog
					echo '<h4 class="text-success col-md-8 col-md-offset-2">Post Deleted</h4>
					<form action="../index.php"><input type="submit" value="Back to Home"></form>';
					//header("Location: index.php");
				}
				else
				{
					echo '<h4 class="text-danger col-md-8 col-md-offset-2">Fail to delete post</h4>';
				}
			}
			else
			{
				echo '<h4 class="text-danger col-md-8 col-md-offset-2">Unable to process form</h4>';
			}
		}
		else
		{
			echo '<h4 class="text-danger col-md-8 col-md-offset-2">Invalid Submission</h4>';
		}
	}
?>
