<?php
    //session_start();
    include '../includes/header.php';
    //set form token to prevent false submission
    $form_token = md5(rand(time(), true));
    //$form_token is randomly generated and encoded
    $_SESSION['form_token'] = $form_token;
    //to keep a common $form_token in the current session across files

    //all input from user will be sent to this file
    $form_action = 'adduser_submit.php';

    //the submit button will appear as 'Add User' button
    $submit_value = 'Add User';

    //include necessary files
    
    include '../includes/user_form.php';   
    //a form for user to input particulars 
    //input will be sent to adduser_submit.php
    include '../includes/footer.php';
?>
