<?php
//output buffering 
	ob_start();
	//include header
	include '../includes/header.php';

	//if not logged in 
	if(!isset($_SESSION['access_level']) || $_SESSION['access_level'] < 1)
	{
		//redirect to login page
		header("Location: login.php");
		exit;
	}
	else
	{
		//check the form has been posted and the session form token is set
		if(isset($_SESSION['form_token'], $_POST['blog_category_id'], $_POST['blog_content_headline'], $_POST['blog_content_text']))
		{
			//check for appropriate length and form of inputs
			if(!is_numeric($_POST['blog_category_id']) || $_POST['blog_category_id']==0)
			{
				echo '<h4 class="text-danger" col-md-8 col-md-offset-2>Blog Category Name is Invalid </h4>';
			}
			elseif(!is_string($_POST['blog_content_headline']) || strlen($_POST['blog_content_headline'])<3)
			{
				echo '<h4 class="text-danger" col-md-8 col-md-offset-2>Blog Headline is invalid </h4>';
			}
			elseif(!is_string($_POST['blog_content_text']) || strlen($_POST['blog_content_text'])<3)
			{
				echo '<h4 class="text-danger" col-md-8 col-md-offset-2>Blog Content is Invalid</h4>';
			}
			else
			{	
				//include db connection
				include '../includes/conn.php';

				if($db) //success
				{
					//escape input strings to prevent injection
					$blog_user_id = $_SESSION['blog_user_id'];
					$blog_category_id = mysqli_real_escape_string($link, $_POST['blog_category_id']);
					$blog_content_headline = mysqli_real_escape_string($link, $_POST['blog_content_headline']);
					$blog_content_text = mysqli_real_escape_string($link, nl2br($_POST['blog_content_text']));

					//create SQL query
					$sql = "INSERT
						INTO
						blog_content(
						blog_user_id,
						blog_category_id,
						blog_content_headline,
						blog_content_text)
						VALUES (
						'{$blog_user_id}',
						'{$blog_category_id}',
						'{$blog_content_headline}',
						'{$blog_content_text}')";

					if(mysqli_query($link, $sql)) //if successfully run the query
					{
						//unset form token
						unset($_SESSION['form_token']);

						echo '<h4 class="text-success col-md-8 col-md-offset-2"><font color=990033>Blog Entry Added</font></h4>';
						//success
					}
					else
					{
						echo '<h4 class="text-danger col-md-8 col-md-offset-2"><font color=990033>Blog Entry Not Added' .mysqli_error($link).'</font></h4>';
					}
				}
				else
				{
					echo '<h4 class="text-danger"><font color=990033>Unable to process form</font></h4>';
				}
			}
		}
		else
		{
			echo '<h4 class="text-danger col-md-8 col-md-offset-2"><font color=990033>Invalid Submission</font></h4>';
		}
	}
?>
