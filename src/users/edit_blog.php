<?php
	//output buffering 
	ob_start();
	//include header
	include '../includes/header.php';

	//check access level
	if(!isset($_SESSION['access_level'], $_SESSION['blog_user_id']))
	{
		header("Location: login.php");
		exit;
	}
	else
	{
		//set form token
		$form_token = uniqid();
		$_SESSION['form_token'] = $form_token;

		//bid (blog id) was given by list_blog.php; check whether it's set
		if(isset($_GET['bid']) && is_numeric($_GET['bid']))
		{
			//include db connection
			include '../includes/conn.php';
			if($db)
			{
				//get categories from db and put into an array
				$categories = array();
				$sql = "SELECT blog_category_id, blog_category_name 
						FROM blog_categories";
				$result = mysqli_query($link, $sql);
				while($row = mysqli_fetch_array($result))
				{
					$categories[$row['blog_category_id']] = $row['blog_category_name'];
				}

				//escape input string to prevent injection
				$blog_content_id = mysqli_real_escape_string($link, $_GET['bid']);
				$sql = "SELECT
					blog_content_id,
					blog_content_headline,
					blog_content_text,
					blog_category_id,
					blog_category_name
					FROM
					blog_content
					JOIN
					blog_categories
					USING(blog_category_id)
					WHERE
					blog_content_id = $blog_content_id";

				//if user is not admin
				if($_SESSION['access_level'] == 1)
				{
					//retrieve user's entries only
					$blog_user_id = mysqli_real_escape_string($link, $_SESSION['blog_user_id']);
					$sql .= " AND blog_user_id=$blog_user_id";
				}

				//run the query 
				$result = mysqli_query($link, $sql) or die(mysqli_error($link));

				//check if there is a blog entry
				if(mysqli_num_rows($result) != 0)
				{
					while($row = mysqli_fetch_array($result))
					{
						$blog_heading = 'Edit Blog';	
						$blog_form_action = 'edit_blog_submit.php';
						$selected = $row['blog_category_id'];
						$blog_content_id = $row['blog_content_id'];
						$blog_content_headline = $row['blog_content_headline'];
						$blog_content_text = $row['blog_content_text'];
						$blog_form_submit_value = 'Confirm';
					}
					//include the blog form 
					include '../includes/blog_form.php';
				}
				else
				{
					echo '<h4 class="text-danger col-md-8 col-md-offset-2">No blog found</h4>';
				}
			}
		}
		else
		{
			//failed to connect to db
			echo '<h4 class="text-danger col-md-8 col-md-offset-2">Unable to complete request</h4>';
		}
	
		include '../includes/footer.php';
	}
?>
