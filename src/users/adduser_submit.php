<?php
//report all errors
error_reporting(E_ALL);

//session_start();

//include the header file 
include '../includes/header.php';

//array to store errors
$errors = array();

//if there is no form token set => error
if(!isset($_SESSION['form_token']))
{
	$errors[] = 'Invalid Form Token';
}
//if not all fields are filled => error
elseif(!isset($_POST['form_token'], $_POST['blog_user_name'], $_POST['blog_user_password'], $_POST['blog_user_password2'], $_POST['blog_user_email']))
{
	$errors[] = 'All fields must be completed';
}
//check if the form token is valid (same as the randomly generated one in adduser.php)
elseif($_SESSION['form_token'] != $_POST['form_token'])
{
	$errors[] = 'You may only post once';
}
//check the length of the user name 
elseif(strlen($_POST['blog_user_name']) < 2 || strlen($_POST['blog_user_name']) > 25)
{
	$errors[] = 'User Name must be between 2 and 25 characters';
}
//check the length of the password 
elseif(strlen($_POST['blog_user_password']) <= 6 || strlen($_POST['blog_user_password']) > 25)
{
	$errors[] = 'Password must be between 6 and 25 characters';
}
//check the length of the users email 
elseif(strlen($_POST['blog_user_email']) < 4 || strlen($_POST['blog_user_email']) > 254)
{
	$errors[] = 'Email must be between 4 and 254 characters';
}
//valid email?
elseif(!preg_match("/^\S+@[\w\d.-]{2,}\.[\w]{2,6}$/iU", $_POST['blog_user_email']))
{
	$errors[] = 'Invalid Email Address';
}

//if everything is fine
else
{
	//escape strings to make sure data input is safe
	$blog_user_name = $_POST['blog_user_name'];
	//encrypt the password using sha1
	$blog_user_password = sha1($_POST['blog_user_password']);
	//$blog_user_password = $blog_user_password;
	//make sure there's no injection char in email input: replacing them with ''
	$blog_user_email =  preg_replace( '((?:\n|\r|\t|%0A|%0D|%08|%09)+)i' , '', $_POST['blog_user_email'] );
	$blog_user_email = $blog_user_email;

	//select db by including 'includes/conn.php'
	include '../includes/conn.php';

	//test for db connection 
	if($db)
	{
		//check for existing username and email
		//define a query
		$sql = "SELECT
			blog_user_name,
			blog_user_email
			FROM
			blog_users
			WHERE
			blog_user_name = '{$blog_user_name}'
			OR
			blog_user_email = '{$blog_user_email}'";
		//send the query
		$result = mysqli_query($link, $sql);
		//define an array storing result of the query
		$row = mysqli_fetch_row($result);
		if($row[0] == $blog_user_name)
		{
			$errors[] = 'User name is already in use';
		}
		elseif($row[1] == $blog_user_email)
		{
			$errors[] = 'Email address already subscribed';
		}
		else
		{

			//define a query: update the table in db
			$sql = "INSERT
				INTO
				blog_users(
				blog_user_name,
				blog_user_password,
				blog_user_email,
				blog_user_access_level)
				VALUES (
				'{$blog_user_name}',
				'{$blog_user_password}',
				'{$blog_user_email}',
				1)";

			//run the query 
			//if successful:
			if(mysqli_query($link, $sql))
			{
				//$message ='Sign up successful.';
				//echo "<script language = 'javascript'> alert($message); </script>";
			}
			else //if query is not successful
			{
				$errors[] = 'User Not Added';
			}
		}
	}
	else //if can't connect to db
	{
		$errors[] = 'Unable to process form';
	}
}

//check if there are any errors in the errors array 
if(sizeof($errors) > 0)
{
	foreach($errors as $err)
	{
		echo '<div class="text-justify col-md-8 col-md-offset-2"><font color=#E0E0E0>'.$err.'</font></div>','<br />';
		//display all the errors
	}
}
else //if no error
{
	echo '<div class="text-justify col-md-8 col-md-offset-2"><font color=#E0E0E0>Register successfully, please sign in to start posting.</font></div><br />';
	
	header("Location: login.php");
}

//include the footer file
include '../includes/footer.php';

?>
