<?php
	//output buffering
	ob_start();

	//begin session
	session_start();
	global $base_dir;
	//if there exists any error: redirect user to the login page
	if(!isset($_SESSION['form_token']))
	{
		$location = 'login.php';
	}
	//check all fields have been posted 
	elseif(!isset($_POST['form_token'], $_POST['blog_user_name'], $_POST['blog_user_password']))
	{
		$location = 'login.php';
	}
	//check the form token is valid
	elseif($_SESSION['form_token'] != $_POST['form_token'])
	{
		$location = 'login.php';
	}
	//check the length of the user name 
	elseif(strlen($_POST['blog_user_name']) < 2 || strlen($_POST['blog_user_name']) > 25)
	{
		$location = 'login.php';
	}
	// check the length of the password
	elseif(strlen($_POST['blog_user_password']) < 8 || strlen($_POST['blog_user_password']) > 25)
	{
		$location = 'login.php';
	}
	else
	{
		//include the db connection 
		include '../includes/conn.php';
		//escape all vars for database use
		$blog_user_name = mysqli_real_escape_string($link, $_POST['blog_user_name']);
		//echo $blog_user_name;
		//encrypt the password using sha1
		$blog_user_password = sha1($_POST['blog_user_password']);
		//prevent injection
		$blog_user_password = mysqli_real_escape_string($link, $blog_user_password);
		//echo $blog_user_password;

		//test for db connection
		//$db was defined in $base_dir.'/includes/conn.php' 
		if($db)
		{
			//check for existing username and password 
			$sql = "SELECT
			blog_user_id,
			blog_user_name,
			blog_user_password,
			blog_user_access_level
			FROM
			blog_users
			WHERE
			blog_user_name = '{$blog_user_name}'
			AND
			blog_user_password = '{$blog_user_password}'";

			$result = mysqli_query($link, $sql);
			//echo 
			//echo $result;
			//echo mysqli_num_rows($result);
			if(mysqli_num_rows($result) != 1) //if no match found
			{
				//echo 'wrong';
				$location = 'login.php'; //redirect to login page
			}
			else
			{
				//echo 'correct';
				//fetch user particulars
				$row = mysqli_fetch_row($result);

				//set access level to that of the registered user
				$_SESSION['access_level'] = $row[3];

				//set user id
				$_SESSION['blog_user_id'] = $row[0];

				//unset form token 
				unset($_SESSION['form_token']);

				//redirect user to index page to begin using the blog
				$location = '../index.php';
			}
		}
	}

	//redirect
	header("Location: $location");

	//flush buffer, send output
	ob_end_flush();

?>
