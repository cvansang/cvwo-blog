<div class="col-md-8 col-md-offset-2">
<div id='new-entry'>
<h2 class = "text-info"><?php echo $blog_heading; ?></h2>
 <script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
 <script>
          tinymce.init({
              selector: "textarea",
              plugins: [
                  "advlist autolink lists link image charmap print preview anchor",
                  "searchreplace visualblocks code fullscreen",
                  "insertdatetime media table contextmenu paste"
              ],
              toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
          });
 </script> 
<style>
	dt {
		color: #E0E0E0;
	}
</style>
<!--Create a form to input blog content, while sending hidden content id and form token-->
<div class='form-group'>
<form action="<?php echo $blog_form_action; ?>" method="post">
<input type="hidden" name="blog_content_id" value="<?php echo isset($blog_content_id) ? $blog_content_id : ''; ?>" />
<input type="hidden" name="form_token" value="<?php echo isset($form_token) ? $form_token : ''; ?>" />
<dl>
<dt>Title</dt>
<dd><input class="form-control" type="text" name="blog_content_headline" value="<?php echo isset($blog_content_headline) ? $blog_content_headline : ''; ?>"/></dd>

<dt>Category</dt>
<!-- Select Category -->
<dd>
<select class="form-control" name="blog_category_id">
<?php
	foreach($categories as $id=>$cat)
	{
		echo "<option value=\"$id\"";
		//mark as selected 
		echo (isset($selected) && $id==$selected) ? ' selected' : '';
		echo ">$cat</option>\n";
	}
?>
</select>
<dd>

<script>
	function ButtonClick_Test()
	{
		document.getElementById("blog_main").value += "<figure><img src=' PASTE LINK HERE '/><figcaption> CAPTION GOES HERE </figcaption></figure>";
	}
</script>

<dt>Content</dt>

<dd>
<textarea name="blog_content_text" rows="20" cols="100" id = "blog_main"><?php echo isset($blog_content_text) ? str_replace('<br />', "", $blog_content_text) : ''; //display existing content?></textarea>
</dd>

<dd><input class="btn btn-primary" type="submit" value="<?php echo $blog_form_submit_value; ?>" /></dd>
</dl>
</form>
</div>
</div>
</div>
