<style>
	dt {
		color: #E0E0E0;
	}
</style>
<!-- Heading -->
<div class="col-md-8 col-md-offset-4">
<div id='sign-up'>
<h2><?php echo isset($heading) ? $heading : ''; ?></h2>	
<!--check if $heading is already set.-->

<form action="<?php echo isset($form_action) ? $form_action : ''; ?>" method="post">
	<!-- data is posted to $form_action -->
<fieldset>
	<h2 class='text-info'> Signup Form </h2>
	
	<dt> Username </dt>
	<div class="form-group"> 
		<input type="text" id="blog_user_name" name="blog_user_name" class="form-control" value="<?php echo isset($blog_user_name) ? $blog_username : ''; ?>" maxlength="20" />
	</div>

	<dt> Password </dt>
	<div class="form-group">
		<input type="password" id="blog_user_password" name="blog_user_password" class="form-control" value="" maxlength="20" />
	</div>

	<dt> Confirm Password </dt>
	<div class="form-group">
		<input type="password" id="blog_user_password2" name="blog_user_password2" class="form-control" value="" maxlength="20" />
	</div>

	<dt> Email Address </dt>
	<div class="form-group">
		<input type="text" id="blog_user_email" name="blog_user_email" class="form-control" value="<?php echo isset($blog_user_email) ? $blog_user_email : ''; ?>" maxlength="254" />
	</div>

	<input type="hidden" name="form_token" value="<?php echo isset($form_token) ? $form_token : ''; ?>" />
	<br>
	<dd>
		<input type="submit" class="btn btn-primary" value="<?php echo isset($submit_value) ? $submit_value : 'Submit'; ?>" />
	</dd>
	</dl>
</fieldset>
</div>
</div>
	
