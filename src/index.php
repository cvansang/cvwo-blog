<?php
	//report all errors
	error_reporting(E_ALL);
	//include header
	include 'includes/header.php';

	//include db connection
	include 'includes/conn.php';

	if($db) //successful connection
	{
		//query to select the latest 5 blog entries
		$sql = "SELECT
			blog_content_id,
			blog_content_headline,
			blog_content_text,
			DATE_FORMAT(blog_content_date, '%b %d %Y') AS blog_content_date,
			blog_category_name,
			blog_user_name
			FROM
			blog_content
			JOIN
			blog_users
			USING(blog_user_id)
			JOIN
			blog_categories
			USING(blog_category_id)
			ORDER BY blog_content_id DESC";

		//run the query
		$result = mysqli_query($link, $sql) or die(mysqli_error($link));

		//create an array to store blog entries
		$blog_array = array();

		//valid resource
		if(mysqli_num_rows($result) != 0) //if the exists some blog
		{
			//copy blogs onto array
			while($row = mysqli_fetch_array($result, MYSQL_ASSOC))
			{
				$blog_array[] = $row;
			}
		}

	}
	else
	{
		echo 'No Blog Entries Available';
	}
?>
<div class="col-md-offset-0">
<div class='entry'>
<?php
	if(sizeof($blog_array) > 0) //if there are some entries
	{
		//display blog entries
		foreach($blog_array as $blog)
		{
			echo "<div class='entry'>";
			echo '<div class="text-info"><h2><a href=view/viewpost.php?cid='.$blog['blog_content_id'].'>'.$blog['blog_content_headline'].'</a></h2></div>';
			echo '<h6><p><span class="text-muted">Added by '.$blog['blog_user_name'].' on '.$blog['blog_content_date'].'</p></h6>';
			echo '<h6><span class="text-muted">'.'Category: '.$blog['blog_category_name'].'</span></h6>';
			
			//create a preview of the entry
			$content = $blog['blog_content_text'];
			$text_limit = 500;
			if (strlen($content) > $text_limit) 
			{
			    // truncate string
			    $shortened = substr($content, 0, $text_limit);

			    // make sure it ends in a word 
			    $content = substr($shortened, 0, strrpos($shortened, ' ')).'...';
			}
			echo '<div class="text-justify"><font color=FFFFFF>'.$content.'</font></div>'; //this is the preview

			//echo the Continue Reading option
			echo '<br>';
			echo '<a href="view/viewpost.php?cid='.$blog['blog_content_id'].'">Read More &#8594</a>'; 
			echo "</div>";
		}
	}
	else
	{
		echo '<h3 class="text-danger col-md-8 col-md-offset-2">No Entry Found</h3>';
	}
?>
</div>
</div>
</div>
<?php
	//include the footer file
	include 'includes/footer.php';

?>
